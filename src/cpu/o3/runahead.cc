#include "runahead.hh"
using namespace std;
using namespace TheISA;

/* Static variables from runahead class used for information transfer from other files */
bool runahead::runahead_mode = false;
unsigned int runahead::PC_causing_runahead = 0x0;
bool runahead::cache_miss = false;
bool runahead::call_squash_all = false;

std::vector<long> runahead::checkpointed_int_reg;
std::vector<long> runahead::checkpointed_float_reg;
std::vector<long> runahead::checkpointed_CC_reg;
std::vector<long> runahead::checkpointed_misc_reg;

std::vector<long> runahead::cache_miss_pc;

std::vector<bool> runahead::Int_invalid;
std::vector<bool> runahead::Float_invalid;
std::vector<bool> runahead::CC_invalid;
std::vector<bool> runahead::Misc_invalid;
std::vector<bool> runahead::store_buf_invalid;
std::vector<cache_element>  runahead::runahead_cache;
MasterPort *runahead::dcache_port;
QueuedSlavePort *runahead::cpuSidePort;

const Cycles runahead::forwardLatency = Cycles(2);


void runahead::runahead_exit(){
      /* Reset the runahead mode boolean if the miss returns! *
      * We need to check if the PC that caused the entry into runahead returned */
      runahead::call_squash_all = true;

}

void runahead::reset_inv_bits(){
      std::fill(Int_invalid.begin(), Int_invalid.end(), false);
      std::fill(Float_invalid.begin(), Float_invalid.end(), false);
      std::fill(CC_invalid.begin(), CC_invalid.end(), false);
      std::fill(Misc_invalid.begin(), Misc_invalid.end(), false);
      std::fill(store_buf_invalid.begin(), store_buf_invalid.end(), false);
}

void runahead::init(DerivO3CPUParams *params){
      /* Initiate the runahead parameters. */

      /* Reserve the space for all the checkpointed reg vectors*/
              checkpointed_int_reg.reserve(TheISA::NumIntRegs);
              checkpointed_float_reg.reserve(TheISA::NumFloatRegs);
              checkpointed_CC_reg.reserve(TheISA::NumCCRegs);
              checkpointed_misc_reg.reserve(TheISA::NumMiscRegs);

     /* Reserve space for valid booleans */
              Int_invalid.reserve(params->numPhysIntRegs);
              Float_invalid.reserve(params->numPhysFloatRegs);
              CC_invalid.reserve(params->numPhysCCRegs);
     /* TODO: Check if NumArchMiscRegs == NumPhysMiscRegs */
              Misc_invalid.reserve(TheISA::NumMiscRegs);

     /* Store buffer invalid queues will be inited and resized from
     * lsq_unit_impl.hh since these resize at times */
                runahead_cache.reserve(64 * 1024 / (16));

}

void runahead::mark_inv(RegIndex arch_reg_idx, bool val, RegIndex phys_reg_idx){
              RegIndex rel_src_reg;

              switch (regIdxToClass(arch_reg_idx, &rel_src_reg)) {

              case IntRegClass:
                            Int_invalid[phys_reg_idx] = val;
                            return;

              case FloatRegClass:
                            Float_invalid[phys_reg_idx] = val;
                            return;

              case CCRegClass:
                            CC_invalid[phys_reg_idx] = val;
                            return;

              case MiscRegClass:
                            Misc_invalid[phys_reg_idx] = val;
                            return;

              }
}

bool runahead::is_inv(RegIndex arch_reg_idx, RegIndex phys_reg_idx){
              RegIndex rel_src_reg;

              switch (regIdxToClass(arch_reg_idx, &rel_src_reg)) {


              case IntRegClass:

                            return Int_invalid[phys_reg_idx] ;

              case FloatRegClass:
                            return Float_invalid[phys_reg_idx] ;

              case CCRegClass:
                            return CC_invalid[phys_reg_idx] ;

              case MiscRegClass:
                            return Misc_invalid[phys_reg_idx] ;

              default:
                            return false;

              }
}

void runahead::checkpoint(RegIndex arch_reg_idx, ThreadContext *TC){
              RegIndex rel_arch_reg;

              if(!runahead_mode){

               switch (regIdxToClass(arch_reg_idx, &rel_arch_reg)) {
                            case IntRegClass:
                                checkpointed_int_reg[rel_arch_reg] =\
                                          TC->readIntReg(rel_arch_reg);
                                          return;

                              case FloatRegClass:
                                   checkpointed_float_reg[rel_arch_reg] =\
                                          TC->readFloatReg(rel_arch_reg);
                                          return;


                              case CCRegClass:
                                   checkpointed_CC_reg[rel_arch_reg] =\
                                                        TC->readCCReg(rel_arch_reg);
                                          return;

                              case MiscRegClass:
                                    checkpointed_misc_reg[rel_arch_reg] =\
                                          TC->readMiscReg(rel_arch_reg);
                                          return;

                              default:
                                panic("rename lookup(): unknown reg class \n");
                            }
              }
}

/* TODO: Send prefetch request to Dcache */
void runahead::runahead_cache_store(Packet * pkt, bool is_source_invalid, Tick time){

      int i = 0;

      for(i = 0; i < (64*1024/16); i++){
            if((pkt->getAddr() & (~(1 << 4))) == runahead_cache[i].tag){
                  std::memcpy(runahead_cache[i].data, pkt->getPtr<uint8_t>(), 64);
                  runahead_cache[i].inv = is_source_invalid;
                      if(pkt->needsResponse()){
                        pkt->makeTimingResponse();
                        cpuSidePort->schedTimingResp(pkt, time + pkt->headerDelay);
                      }
                  return;
            }
      }

      /* It means we had a miss and we need to delete a block. We just have
      * random replacement policy as of now */
      unsigned int index_to_delete = rand()%(64 * 1024/16);
      std::memcpy(runahead_cache[index_to_delete].data, pkt->getPtr<uint8_t>(), 64);
      runahead_cache[index_to_delete].tag = (pkt->getAddr() & (~(1 << 4)));
      runahead_cache[index_to_delete].inv = is_source_invalid;

      if(!is_source_invalid){
            /* Send a prefetch request to Data cache since we might be needing
            this data in future. Of course, we need to change the settings a
            bit. Well this is the core of runahead */
            pkt->cmd = MemCmd::SoftPFReq;            
            dcache_port->sendTimingReq(pkt);
      }
}

void runahead::runahead_cache_load(Packet *pkt, RegIndex dest_reg, \
                                          RegIndex phys_reg_idx, Tick time){

      int i = 0;

      for(i = 0; i < (64*1024/16); i++){
            if((pkt->getAddr() & (~(1 << 4))) == runahead_cache[i].tag){
                  std::memcpy(pkt->getPtr<uint8_t>(), runahead_cache[i].data, 64);
                  mark_inv(dest_reg, runahead_cache[i].inv, phys_reg_idx);
                  if(pkt->needsResponse()){
                          pkt->makeTimingResponse();
                          cpuSidePort->schedTimingResp(pkt, time + pkt->headerDelay);
                  }
                  return;
            }
      }

      /* Send this to Dcache if we missed it in runahead *
      * TODO: retry if dcache is blocked */
      dcache_port->sendTimingReq(pkt);

}
