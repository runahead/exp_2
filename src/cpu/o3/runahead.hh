#ifndef RUNAHEAD_MODE
#define RUNAHEAD_MODE

#include "cpu/reg_class.hh"
#include "cpu/o3/thread_state.hh"
#include "params/DerivO3CPU.hh"
#include "mem/port.hh"
#include "mem/packet.hh"
#include "mem/qport.hh"
#include "cpu/base.hh"
#include "cpu/thread_context.hh"
#include "sim/system.hh"
#include "base/misc.hh"
#include "base/trace.hh"
#include "sim/clocked_object.hh"
#include "sim/sim_exit.hh"
#include "base/types.hh"

#define RUNAHEAD_CACHE_ACCESS_TIME 2


using namespace std;
using namespace TheISA;


class cache_element {
public:
        unsigned int tag;
        int data[4];
        bool inv;
};


class runahead {

        public:
        /* This boolean if true denotes that we are currently in runahead mode
         * Should be cleared once we exit runahead*/
        static bool runahead_mode;

        /* This boolean is set when we are going to exit runahead mode and we
        * request the commit stage to start flushing the pipeline for us via
        * this boolean */
        static bool call_squash_all;

        /* This stores the instruction that caused the cache miss and put us in runahead
        * Should be cleared once we exit runahead*/
        static unsigned int PC_causing_runahead;

        /* This boolean denotes that a cache miss has occured, should be cleared
        * once we exit from runahead mode */
        static bool cache_miss;

        /* This function resets the runahead booleans *
        * TODO: include restoration of architectural state */
        static void runahead_exit();

        /* The init function for runahead */
        static void init(DerivO3CPUParams *params);

        /* Checkpointed register files */
        /* Int checkpointed register files */
        static std::vector<long> checkpointed_int_reg;

        /* Float checkpointed register files */
        static std::vector<long> checkpointed_float_reg;

        /* CC checkpointed register files */
        static std::vector<long> checkpointed_CC_reg;

        /* MISC checkpointed register files */
        static std::vector<long> checkpointed_misc_reg;

        /* Function called to update the checkpointed architectural file */
        static void checkpoint(RegIndex arch_reg_idx, ThreadContext *TC);

        /* We will have a list of Addresses for which Last level cache miss has
        * occured for us to propoagate bogus values */
        static std::vector<long> cache_miss_pc;

        /* The invalid bits for the physical regs */
        static std::vector<bool> Int_invalid;
        static std::vector<bool> Float_invalid;
        static std::vector<bool> CC_invalid;
        static std::vector<bool> Misc_invalid;

        static std::vector<bool> store_buf_invalid;
        static void mark_inv(RegIndex arch_reg_idx, bool val, RegIndex phys_reg_idx);
        static bool is_inv(RegIndex arch_reg_idx, RegIndex phys_reg_idx);

        static void reset_inv_bits();

        /* The actual runahead cache */
        static std::vector<cache_element> runahead_cache;

        /* We need this port to forward misses */
        static MasterPort *dcache_port;

        /* Access to runahead cache, simulate delay now for 1 cycle */
        static void runahead_cache_store(Packet *pkt, bool is_source_invalid, Tick time);

        static void runahead_cache_load(Packet *pkt, RegIndex dest_idx, RegIndex phys_reg_idx, Tick time);

        static QueuedSlavePort *cpuSidePort;

        static const Cycles forwardLatency;


};

#endif
